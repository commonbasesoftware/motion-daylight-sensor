/*
 * messaging.h
 *
 *  Created on: Oct 13, 2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_MESSAGING_MESSAGING_H_
#define SRC_MESSAGING_MESSAGING_H_

// functions defined in library
extern ILB_HAL_StatusTypeDef messaging_sendMessage(ILBMessageStruct *m, void (*callback)(ILBMessageStruct* m), uint16_t timeoutMS, void (*timeoutCallback)(void));
extern ILB_HAL_StatusTypeDef messaging_respondErrorMessage(ILBMessageStruct* m, ILBErrorTypeDef error);
extern uint8_t addressing_getAssignedAddress(void);

#endif /* SRC_MESSAGING_MESSAGING_H_ */
