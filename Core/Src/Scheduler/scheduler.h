/*
 * scheduler.h
 *
 *  Created on: 15.10.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_SCHEDULER_SCHEDULER_H_
#define SRC_SCHEDULER_SCHEDULER_H_

//
//	Get access to scheduler functions
//
extern ILB_HAL_StatusTypeDef scheduler_registerTimerFunction(void (*timeoutHandler)(void), uint16_t timeout);

#endif /* SRC_SCHEDULER_SCHEDULER_H_ */
