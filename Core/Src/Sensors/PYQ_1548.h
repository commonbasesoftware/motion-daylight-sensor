/*
 * PYQ_1548.h
 *
 *  Created on: Jun 19, 2019
 *      Author: A.Niggebaum
 */

#ifndef EXCELTIAS_PYQ_1548_H_
#define EXCELTIAS_PYQ_1548_H_

#include "main.h"

typedef enum PYQ_1548__Config_PulseMode{
	PYQ_1548_CONFIG_PULSE_CHANGE_SIGN = 0x00,				//!< requires change of sign for motion criteria to be met
	PYQ_1548_CONFIG_PULSE_CONSTANT_SIGN = 0x01				//!< requires no change of sign for motion criteria to be met
} PYQ_1548_Config_PulseModeTypeDef;

typedef enum PYQ_1548_Config_FilterCutoff {
	PYQ_1548_CONFIG_FILTER_CUTOFF_04HZ = 0x00,				//!< sets the high pass filter cutoff to 0.4Hz
	PYQ_1548_CONFIG_FILTER_CUTOFF_02HZ = 0x01				//!< sets the high pass filter cutoff to 0.2Hz
} PYQ_1548_Config_FilterCutoffTypeDef;

typedef enum PYQ_1548_Config_FilterSource {
	PYQ_1548_CONFIG_FILTER_SOURCE_BPF = 0x00,				//!< selects the high pass filter as the source voltage
	PYQ_1548_CONFIG_FILTER_SOURCE_LPF = 0x01,				//!< selects the low pass filter as the source voltage
	PYQ_1548_CONFIG_FILTER_SOURCE_TEMP = 0x03				//!< selects the temperature sensor as the source voltage
} PYQ_1548_Config_FilterSourceTypeDef;

typedef enum PYQ_1548_Config_OperationMode {
	PYQ_1548_CONFIG_OPERATION_MODE_FORECED_READOUT = 0x00,	//!< No interrupts are generated, the device needs to be read out manually
	PYQ_1548_CONFIG_OPERATION_MODE_INTERRUPT = 0x01,		//!< an interrupt is generated every (approx.) 14ms (512 clock cycles)
	PYQ_1548_CONFIG_OPERATION_MODE_WAKEUP = 0x02			//!< an interrupt is generated when the motion criteria are met
} PYQ_1548_Config_OperationModeTypeDef;

typedef struct {
	uint8_t sensitivity;									//!< detection threshold
	uint8_t blindTime;										//!< blind time: 0.5s + value (0-15) * 0.5s
	uint8_t pulseCounter;									//!< number of pulses: 1 + value (0-3)
	uint8_t windowTime;										//!< time window in which number of pulses will trigger event: 2s + value (0-3) * 2s
	PYQ_1548_Config_OperationModeTypeDef operationMode;		//!< operation mode of the device
	PYQ_1548_Config_FilterSourceTypeDef filterSource;		//!< filter source used
	PYQ_1548_Config_FilterCutoffTypeDef filterCutOff;		//!< cutoff for filter
	PYQ_1548_Config_PulseModeTypeDef pulseMode;				//!< pulse used (change of sign or no change of sign)
} PYQ1548_Configuration_t;

/*
 * Function prototypes
 */

void PYQ1548_init(void);
void PYQ1548_updateFromRegister(void);

void PYQ1548_transmitSettings(PYQ1548_Configuration_t* configuration);
void PYQ1548_clearInterrupt(void);
void PYQ1548_readFullStatus(uint16_t* statusBits, uint32_t* configBits);
void PYQ1548_extractConfiguration(uint32_t* configBits, PYQ1548_Configuration_t* configuration);
int16_t PYQ1548_GetSignedReading(uint16_t statusBits);

#endif /* EXCELTIAS_PYQ_1548_H_ */
