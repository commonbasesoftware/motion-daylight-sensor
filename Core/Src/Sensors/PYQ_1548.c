/*
 * PYQ_1548.c
 *
 *  Created on: Jun 19, 2019
 *      Author: A.Niggebaum
 *
 * @file PYQ_1548.c
 *
 * @description This file provides functionality for the Excelitas PYQ1546 DigiPyro sensor. It requires one GPIO pin with the name "SERIAL_IN" and
 * one pin with the name "DIRECT_LINK" to communicate with the sensor.
 * DIRECT_LINK must be configured as rising edge interrupt
 * SERIAL_IN must be configured as GPIO output
 */

#include "PYQ_1548_timings.h"

/*
 * Define bit masks for configuration
 */
// number of bits in configuration
#define PYQ_1548_NOFBITSINCONFIG			25

// bit masks
#define PYQ_1548_CONFIG_MASK_SENSITIVITY	0x1FE0000
#define PYQ_1548_CONFIG_MASK_BLINDTIME		0x1E000
#define PYQ_1548_CONFIG_MASK_PULSECOUNTER	0x1800
#define PYQ_1548_CONFIG_MASK_WINDOWTIME		0x600

// bit shifts
#define PYQ_1548_CONFIG_SHIFT_SENSITIVITY		17
#define PYQ_1548_CONFIG_SHIFT_BLINDTIME			13
#define PYQ_1548_CONFIG_SHIFT_PULSECOUNTER		11
#define PYQ_1548_CONFIG_SHIFT_WINDOWTIME		9
#define PYQ_1548_CONFIG_SHIFT_OPERATIONMODE		7
#define PYQ_1548_CONFIG_SHIFT_FILTERSEROUCE		5
#define PYQ_1548_CONFIG_SHIFT_HIGHPASSCUTOFF	2
#define PYQ_1548_CONFIG_SHIFT_PULSEMODE			0

// value readout
#define PYQ_1548_OOR_FLAG						0x4000		//!< out of range flag
#define PYQ_1548_COMPLEMENT_SIGN				0x2000		//!< complement sign mas
#define PYQ_1548_COMPLEMENT_MASK				0x1FFF		//!< value of 14 bit 2 compelement reading

/*
 * Includes
 */
#include "PYQ_1548.h"
#include "gpio.h"

#include "../HWFunctions/memoryLocations.h"
#include "../HWFunctions/memory.h"
#include "../Register/register.h"

//
//	Private function prototypes
//
void generateConfigurationBits(PYQ1548_Configuration_t* configuration, uint32_t* targetVariable);
void pullRegisterToConfiguration(PYQ1548_Configuration_t* pConfig);
void pushConfigurationToRegister(PYQ1548_Configuration_t* pConfig);
void writeRegisterToMemory(void);


//
//	Private variables
//
static PYQ1548_Configuration_t motionSensorConfig;

//
//	Interface functions
//
void PYQ1548_init(void)
{

	// read settings from memory
	uint32_t sensitivity = 0;
	uint32_t settings1 = 0;
	uint32_t settings2 = 0;

	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_MOTION_SENSITIVITY, &sensitivity);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_MOTION_SETTINGS_1, &settings1);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_MOTION_SETTINGS_2, &settings2);

	// check if we need to load defaults
	if (sensitivity + settings1 + settings2 == 0)
	{
		motionSensorConfig.blindTime = 1;
		motionSensorConfig.filterCutOff = PYQ_1548_CONFIG_FILTER_CUTOFF_02HZ;
		motionSensorConfig.filterSource = PYQ_1548_CONFIG_FILTER_SOURCE_BPF;
		motionSensorConfig.operationMode = PYQ_1548_CONFIG_OPERATION_MODE_WAKEUP;
		motionSensorConfig.pulseCounter = 0;
		motionSensorConfig.pulseMode = PYQ_1548_CONFIG_PULSE_CHANGE_SIGN;
		motionSensorConfig.sensitivity = 10;
		motionSensorConfig.windowTime = 1;

		// update the register
		pushConfigurationToRegister(&motionSensorConfig);

		// store register in memory
		writeRegisterToMemory();
	} else {
		// fill register
		registerSettings[REGISTER_SETTINGS_MOTION_SENSITIVITY] = sensitivity;
		registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_1] = settings1;
		registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_2] = settings2;

		// update the settings
		pullRegisterToConfiguration(&motionSensorConfig);
	}

	// transmit settings
	PYQ1548_transmitSettings(&motionSensorConfig);

	// clear the interrupt
	PYQ1548_clearInterrupt();
}

/**
 * Transmit the configuration to the device. This function is blocking
 *
 * disable optimisation of this function to allow hold times based on cycle time
 */
void __attribute__((optimize("O0"))) PYQ1548_transmitSettings(PYQ1548_Configuration_t* configuration)
{

	// get interrupt mask register
	uint32_t maskTMP = EXTI->IMR;
	// unset all masks: disable all interrupts
	EXTI->IMR = 0;

	// get the configuration bit sequence
	uint32_t configurationSequence = 0;
	generateConfigurationBits(configuration, &configurationSequence);

	/*
	 * Begin of transmission
	 */
	// transmit the bits. Note that we need to start with the hightest bit!!
	uint8_t bitCounter = PYQ_1548_NOFBITSINCONFIG;
	uint16_t bitHoldCounter = 0;
	// make sure the line is low
	SERIAL_IN_GPIO_Port->BSRR = SERIAL_IN_Pin;
	while (bitCounter > 0) {
		// generate the low to high transition
		SERIAL_IN_GPIO_Port->BSRR = SERIAL_IN_Pin;
		// transmit the bit
		if (((configurationSequence >> (bitCounter - 1)) & 0x01) != 0x01) {
			SERIAL_IN_GPIO_Port->BRR = SERIAL_IN_Pin;
		}
		// hold the bit
		bitHoldCounter = PYQ_1548_BIT_HOLD_TICKS;
		while (bitHoldCounter > 0) {
			bitHoldCounter--;
		}
		// reset line
		SERIAL_IN_GPIO_Port->BRR = SERIAL_IN_Pin;
		bitCounter--;
	}

	// re-enable interrupts
	EXTI->IMR = maskTMP;
}

/**
 * Clear the interrupt on the direct link line
 *
 * Optimisation is disabled to allow counting in us area
 */
void __attribute__((optimize("O0"))) PYQ1548_clearInterrupt(void)
{
	// to clear the interrupt, the line must be pulled down for at least one system clock cycle of the PYQ1548
	// set output to pull down

	// set mode to output. This sets the line to low
	//DIRECT_LINK_GPIO_Port->MODER |= (0x01 << ((DIRECT_LINK_Pin - 1) * 2));
	GPIO_InitTypeDef pinOutput;
	pinOutput.Pin = DIRECT_LINK_Pin;
	pinOutput.Mode = GPIO_MODE_OUTPUT_PP;
	pinOutput.Pull = GPIO_PULLDOWN;
	HAL_GPIO_DeInit(DIRECT_LINK_GPIO_Port, DIRECT_LINK_Pin);
	HAL_GPIO_Init(DIRECT_LINK_GPIO_Port, &pinOutput);


	// wait clock cycles
	uint16_t tmp = PYQ_1548_BIT_HOLD_TICKS;
	while (tmp > 0) {
		tmp--;
	}

	// reset type to input
	//DIRECT_LINK_GPIO_Port->MODER &= ~(0x03 << ((DIRECT_LINK_Pin-1)*2));

	pinOutput.Pin = DIRECT_LINK_Pin;
	pinOutput.Mode = GPIO_MODE_IT_RISING;
	pinOutput.Pull = GPIO_NOPULL;
	HAL_GPIO_DeInit(DIRECT_LINK_GPIO_Port, DIRECT_LINK_Pin);
	HAL_GPIO_Init(DIRECT_LINK_GPIO_Port, &pinOutput);
}

/**
 * Read the ADC voltages and configuration of the PIR element
 *
 * @param statusBits pointer to 16bit variable to hold the satus and ADC values
 * @param configBits pointer to 32bit variable to hold the configuration bits
 */
void __attribute__((optimize("O0"))) PYQ1548_readFullStatus(uint16_t* statusBits, uint32_t* configBits)
{

	// prepare variables
	uint8_t bitsToRead = 40;
	// reset target variable
	uint32_t tmpConfigRead = 0;
	uint16_t tmpStatusRead = 0;

	// deactivate the interrupt
	HAL_NVIC_DisableIRQ(DIRECT_LINK_EXTI_IRQn);

	// start the readout process
	// reset pin configuration
	HAL_GPIO_DeInit(DIRECT_LINK_GPIO_Port, DIRECT_LINK_Pin);

	// send setup sequence
	// pull line low
	DIRECT_LINK_GPIO_Port->ODR &= ~(0x01 << (DIRECT_LINK_Pin-1));
	// configure pin as general purpose output
	DIRECT_LINK_GPIO_Port->MODER &= ~(0x03 << ((DIRECT_LINK_Pin-1) * 2));
	DIRECT_LINK_GPIO_Port->MODER |= (0x01 << ((DIRECT_LINK_Pin-1)*2));

	// configure line as push pull
	DIRECT_LINK_GPIO_Port->OTYPER &= ~(0x01 << (DIRECT_LINK_Pin-1));

	// pull line up
	DIRECT_LINK_GPIO_Port->ODR |= (0x01 << (DIRECT_LINK_Pin-1));

	// wait the setup time
	uint16_t tmp = PYQ_1548_SETUP_HOLD_TICKS;
	// wait setup time time
	while (tmp > 0) {
		tmp--;
	}

	while (bitsToRead > 0) {

		// pull the line low.
		// this needs to be done as the first action to prevent accidental changes on the line
		DIRECT_LINK_GPIO_Port->ODR &= ~(0x01 << (DIRECT_LINK_Pin-1));

		// configure pin as general purpose output
		DIRECT_LINK_GPIO_Port->MODER &= ~(0x03 << ((DIRECT_LINK_Pin-1) * 2));
		DIRECT_LINK_GPIO_Port->MODER |= (0x01 << ((DIRECT_LINK_Pin-1)*2));

		// configure line as push pull
		DIRECT_LINK_GPIO_Port->OTYPER &= ~(0x01 << (DIRECT_LINK_Pin-1));

		// wait a bit
		uint8_t waiting = PYQ_1548_READOUT_HOLD_SETUP_TICS;
		while (waiting > 0) {
			waiting--;
		}

		// pull the line high
		DIRECT_LINK_GPIO_Port->ODR |= (0x01 << (DIRECT_LINK_Pin-1));
		// wait a bit
		waiting = PYQ_1548_READOUT_HOLD_SETUP_TICS;
		while (waiting > 0) {
			waiting--;
		}

		// set line to open drain
		DIRECT_LINK_GPIO_Port->OTYPER |= (0x01 << (DIRECT_LINK_Pin-1));

		// set the pin to input type
		DIRECT_LINK_GPIO_Port->MODER &= ~(0x03 << ((DIRECT_LINK_Pin-1) * 2));

		// wait a bit
		tmp = PYQ_1548_READOUT_HOLD_TICKS;
		while (tmp > 0) {
			tmp--;
		}

		// read pin input
		if (bitsToRead > 25) {
			tmpStatusRead = tmpStatusRead << 1;
			if((DIRECT_LINK_GPIO_Port->IDR & DIRECT_LINK_Pin) != (uint32_t)GPIO_PIN_RESET) {
				tmpStatusRead |= 0x01;
			}
		} else {
			tmpConfigRead = tmpConfigRead << 1;
			if((DIRECT_LINK_GPIO_Port->IDR & DIRECT_LINK_Pin) != (uint32_t)GPIO_PIN_RESET) {
				tmpConfigRead |= 0x01;
			}
		}
		bitsToRead--;
	}

	// stop the readout

	// pull the line low
	DIRECT_LINK_GPIO_Port->ODR &= ~(0x01 << (DIRECT_LINK_Pin-1));

	// configure pin as general purpose output
	DIRECT_LINK_GPIO_Port->MODER &= ~(0x03 << ((DIRECT_LINK_Pin-1) * 2));
	DIRECT_LINK_GPIO_Port->MODER |= (0x01 << ((DIRECT_LINK_Pin-1)*2));

	// configure line as push pull
	DIRECT_LINK_GPIO_Port->OTYPER &= ~(0x01 << (DIRECT_LINK_Pin-1));

	tmp = PYQ_1548_READOUT_STOP_TICKS;
	while (tmp > 0) {
		tmp--;
	}

	// re-configure the pin as initially
	HAL_GPIO_DeInit(DIRECT_LINK_GPIO_Port, DIRECT_LINK_Pin);
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = DIRECT_LINK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DIRECT_LINK_GPIO_Port, &GPIO_InitStruct);

	// re-activate the interrupt
	HAL_NVIC_EnableIRQ(DIRECT_LINK_EXTI_IRQn);

	// store output
	*statusBits = tmpStatusRead;
	*configBits = tmpConfigRead;
}

/**
 * Extracts the configuration information out of the raw configuration bytes and stores it in the provided structure
 *
 * @param configBits pointer to raw configuration buts
 * @param configuration pointer to configuration structure
 */
void PYQ1548_extractConfiguration(uint32_t* configBits, PYQ1548_Configuration_t* configuration)
{
	// extract sensitivity
	configuration->sensitivity 		= ((*configBits >> 17) & 0xFF);
	configuration->blindTime 		= ((*configBits >> 13) & 0x0F);
	configuration->pulseCounter 	= ((*configBits >> 11) & 0x03);
	configuration->windowTime 		= ((*configBits >> 9) & 0x03);
	configuration->operationMode 	= ((*configBits >> 7) & 0x03);
	configuration->filterSource 	= ((*configBits >>5) & 0x03);
	configuration->filterCutOff 	= ((*configBits >> 2) & 0x01);
	configuration->pulseMode 		= (*configBits & 0x01);
}

/**
 * Convert the status bits to a signed reading
 */
int16_t PYQ1548_GetSignedReading(uint16_t statusBits)
{
	// overflow condition when bit is 0, valid if 1
	if ((statusBits & PYQ_1548_OOR_FLAG) != PYQ_1548_OOR_FLAG)
	{
		return 0xFFFF;
	}
	if ((statusBits & PYQ_1548_COMPLEMENT_SIGN) == PYQ_1548_COMPLEMENT_SIGN)
	{
		return -(~statusBits & PYQ_1548_COMPLEMENT_MASK) - 1;
	}
	else
	{
		return (statusBits & PYQ_1548_COMPLEMENT_MASK);
	}
}

//
//	Private function implementation
//

/**
 * Generate a bit sequence out of a configuration
 */
void generateConfigurationBits(PYQ1548_Configuration_t* configuration, uint32_t* targetVariable)
{

	// generate temporary variable
	uint32_t tmpVar = (0x02 << 3);

	// fill variable with values
	tmpVar |= ((uint32_t)(configuration->pulseMode) << PYQ_1548_CONFIG_SHIFT_PULSEMODE);
	tmpVar |= ((uint32_t)configuration->filterCutOff << PYQ_1548_CONFIG_SHIFT_HIGHPASSCUTOFF);
	tmpVar |= ((uint32_t)configuration->filterSource << PYQ_1548_CONFIG_SHIFT_FILTERSEROUCE);
	tmpVar |= ((uint32_t)configuration->operationMode << PYQ_1548_CONFIG_SHIFT_OPERATIONMODE);
	tmpVar |= ((uint32_t)(configuration->windowTime << PYQ_1548_CONFIG_SHIFT_WINDOWTIME) & PYQ_1548_CONFIG_MASK_WINDOWTIME);
	tmpVar |= ((uint32_t)(configuration->pulseCounter << PYQ_1548_CONFIG_SHIFT_PULSECOUNTER) & PYQ_1548_CONFIG_MASK_PULSECOUNTER);
	tmpVar |= ((uint32_t)(configuration->blindTime << PYQ_1548_CONFIG_SHIFT_BLINDTIME) & PYQ_1548_CONFIG_MASK_BLINDTIME);
	tmpVar |= ((uint32_t)(configuration->sensitivity << PYQ_1548_CONFIG_SHIFT_SENSITIVITY)  & PYQ_1548_CONFIG_MASK_SENSITIVITY);

	// assign to the target variable
	*targetVariable = tmpVar;
}

/**
 * Pull the settings from the register and push it to the configuration
 *
 * @param config Pointer to Configuration file
 */
void pullRegisterToConfiguration(PYQ1548_Configuration_t* pConfig)
{
	pConfig->sensitivity = registerSettings[REGISTER_SETTINGS_MOTION_SENSITIVITY];
	uint8_t settings1 = registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_1];
	uint8_t settings2 = registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_2];
	pConfig->windowTime = (settings1 & 0x03);
	pConfig->pulseCounter = ((settings1 >> 2) & 0x03);
	pConfig->blindTime = ((settings1 >> 4) & 0x0F);
	pConfig->filterCutOff = (settings2 & 0x01);
	pConfig->filterSource = ((settings2 >> 1) & 0x01);
	pConfig->pulseMode = ((settings2 >> 2) & 0x01);
	pConfig->operationMode = PYQ_1548_CONFIG_OPERATION_MODE_WAKEUP;
}

/**
 * Push the configuration to the register
 *
 * The register must be written independently to memory
 * @param config
 */
void pushConfigurationToRegister(PYQ1548_Configuration_t* pConfig)
{
	registerSettings[REGISTER_SETTINGS_MOTION_SENSITIVITY] = pConfig->sensitivity;
	uint8_t tmp = 0;
	tmp |= (pConfig->windowTime);
	tmp |= (pConfig->pulseCounter << 2);
	tmp |= (pConfig->blindTime << 4);
	registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_1] = tmp;
	tmp = 0;
	tmp |= (pConfig->filterCutOff);
	tmp |= (pConfig->filterSource << 1);
	tmp |= (pConfig->pulseMode << 2);
	registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_2] = tmp;

}

/**
 * Write the register to memory
 */
void writeRegisterToMemory(void)
{
	uint32_t tmp = registerSettings[REGISTER_SETTINGS_MOTION_SENSITIVITY];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_MOTION_SENSITIVITY, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_1];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_MOTION_SETTINGS_1, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_SENSOR_SETTINGS_2];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_MOTION_SETTINGS_2, &tmp);
}

/**
 * Pull settings from register and write them to the sensor. Save register to memory
 */
void PYQ1548_updateFromRegister(void)
{
	// pull settings
	pullRegisterToConfiguration(&motionSensorConfig);
	// write to sensor
	PYQ1548_transmitSettings(&motionSensorConfig);
	// store settings in eeprom
	writeRegisterToMemory();
}
