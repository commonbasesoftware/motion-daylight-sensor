/*
 * PYQ_1548_timings.h
 *
 *  Created on: 10.01.2020
 *      Author: A.Niggebaum
 */

#ifndef EXCELITAS_PYQ_1548_TIMINGS_H_
#define EXCELITAS_PYQ_1548_TIMINGS_H_

#define PYQ_1548_BIT_HOLD_TICKS					(150)			//!< number of microcontroller ticks to hold a bit for writing. Minimum is 72us. Non multiplied number works for 8MHz clock
#define PYQ_1548_SETUP_HOLD_TICKS				(150)				//!< number of ticks to wait for readout setup. Must between 110 and 150us
#define PYQ_1548_READOUT_HOLD_TICKS				(8)
#define PYQ_1548_READOUT_STOP_TICKS				(400L*2)			//!< must be equivalent to at least 145us
#define PYQ_1548_READOUT_HOLD_SETUP_TICS		1					//!< low and high setup times to mark start of new bit.

#endif /* EXCELITAS_PYQ_1548_TIMINGS_H_ */
