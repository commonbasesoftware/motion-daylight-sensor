/*
 * veml6030.c
 *
 *  Created on: Sep 22, 2021
 *      Author: A.Niggebaum
 */

#include <stdint.h>
#include "i2c.h"

#include "ilb.h"
#include "veml6030.h"
#include "../Register/register.h"
#include "../Scheduler/scheduler.h"

#define VEML6030_ADDRESS	0x20

#define VEML6030_LUX_FACTOR_1				1		//!< factor to convert to 1 lux as unit
#define VEML6030_LUX_FACTOR_01				10		//!< factor to convert to 0.1 lux as unit

#define READOUT_MINIMAL_HANDLE_TIME_MS	5			//!< minimal time in ms when the handle should be execute immediately again
#define READOUT_EXTRA_MS				10			//!< extra time in ms given to the current integration time before we read again
#define VEML_6030_NOMINAL_CYCLE_TIME_MS	1000		//!< nominal time between readouts in ms
#define VEML_6030_MINIMAL_SETTINGS_CHANGE_TIME_MS	10	//!< minimal cycle time to change settings

#define GAIN_INDEX_MAX					3
#define GAIN_INDEX_MIN					0
#define GAIN_INDEX_DEFAULT				0
#define INTEGRATION_INDEX_MAX			5
#define INTEGRATION_INDEX_MIN			0
#define INTEGRATION_INDEX_DEFAULT		2

#define SENSITIVITY_LUT_DIVISOR			10000		//!< used in conjunction with the values in the sensitivityLookup table. The factor is calculated by dividing the table values by this factor

enum VEML6030CommandCodes {
	VEML6030_ALS_CONF	= 0x00,
	VEML6030_ALS_WH		= 0x01,
	VEML6030_ALS_WL		= 0x02,
	VEML6030_PWR_SAV	= 0x03,
	VEML6030_ALS		= 0x04,
	VEML6030_WHITE		= 0x05,
	VEML6030_INT		= 0x06
};

typedef enum GainSettings {
	GAIN1	= 0x0000,
	GAIN2 	= 0x0800,
	GAIN1_8	= 0x1000,
	GAIN1_4	= 0x1800,
} GainSettings_t;

typedef enum IntegrationTime {
	INTEGRATION25ms		= 0x300,
	INTEGRATION50ms		= 0x200,
	INTEGRATION100ms 	= 0x000,
	INTEGRATION200ms	= 0x040,
	INTEGRATION400ms	= 0x080,
	INTEGRATION800ms	= 0x0C0,
} IntegrationTime_t;

typedef enum VEML6030State {
	SensorOn			= 0x00,
	SensorOff			= 0x01
} VEML6030State_t;

typedef struct VEMLSettings {
	GainSettings_t gain;
	IntegrationTime_t integration;
	VEML6030State_t state;
} VEMLSettings_t;

typedef enum VEML6030WorkerStatus {
	VEML6030_IDLE,
	VEML6030_RUNNING,
	VEML6030_ADJUST,
	VEML6030_SHUTDOWN,
} VEML6030Status_t;

static const uint16_t integrationTimeList[] = {INTEGRATION25ms, INTEGRATION50ms, INTEGRATION100ms, INTEGRATION200ms, INTEGRATION400ms, INTEGRATION800ms};
static const uint16_t gainSettingsList[] 	= {GAIN1_8, GAIN1_4, GAIN1, GAIN2};
static const uint16_t integrationTimeMS[]	= {25, 50, 100, 200, 400, 800};

//
//	Private variables
//
static VEMLSettings_t settings;
static uint32_t veml6030_lightLevelLUX = 0;
static VEML6030Status_t algorithmStatus = VEML6030_IDLE;

// variables for main algorithm
static int8_t integrationIndex = INTEGRATION_INDEX_DEFAULT; 	// set to 100ms
static int8_t gainIndex = GAIN_INDEX_DEFAULT;		 	// set to 1/8

/**
 * Senstitivity look up table
 * axis 1 [4]: gain2, gain1, gain1_4, gain1_8
 * axis 2 [6]: 800ms, 400ms, 200ms, 100ms, 50ms, 25ms
 */
static const uint16_t sensitivityLookup[6][4] = {
		{18432, 9216, 2304, 1152},
		{9216, 4608, 1152, 576},
		{4608, 2304, 576, 288},
		{2304, 1152, 288, 144},
		{1152, 576, 144, 72},
		{576, 288, 72, 36}
};

//
//	Function prototypes
//
void changeSettings(void);

void veml6030_handle(void);

//
//	private functions
//
/**
 * Apply the currently active scaling factor the the counts.
 *
 * Currently active integration and gain setting are tracked independently
 *
 * @param counts
 */
void applyIntensityLookup(uint32_t* counts)
{
	// apply value. We relay on external range checks!!
	*counts = ((*counts) * (uint32_t)sensitivityLookup[integrationIndex][gainIndex]) / (uint32_t)SENSITIVITY_LUT_DIVISOR;
}

/**
 * Takes the counts (must be intensity corrected) and pushes the value to the lux variable
 *
 * @param cts
 */
void calculateLuxLevel(uint16_t cts)
{
	// TODO: apply correction if required
	// prepare value for register
	uint16_t registerValue = 0;
	// apply the scaling factor
	veml6030_lightLevelLUX = cts * VEML6030_LUX_FACTOR_1;
	applyIntensityLookup(&veml6030_lightLevelLUX);
	// check range
	if (veml6030_lightLevelLUX < 500)
	{
		// re-calculate for higher resolution
		veml6030_lightLevelLUX = (uint16_t)cts * VEML6030_LUX_FACTOR_01;
		applyIntensityLookup(&veml6030_lightLevelLUX);
		registerValue |= VEML6030_RESOLUTION_FLAG_01;
	}
	registerValue |= (veml6030_lightLevelLUX & 0xFFFF);
	// push to register
	registerReadings[REGISTER_READING_DAYLIGHT_UNIFLTERED_MSB] = ((registerValue >> 8) & 0xFF);
	registerReadings[REGISTER_READING_DAYLIGHT_UNFILTERED_LSB] = (registerValue & 0xFF);

	registerReadings[REGISTER_READING_DAYLIGHT_FILTERED_MSB] = ((registerValue >> 8) & 0xFF);
	registerReadings[REGISTER_READING_DAYLIGHT_FILTERED_LSB] = (registerValue & 0xFF);
}

/**
 * Write the currently active settings to the sensor
 *
 * @return ILB_HAL_OK if communication is success
 */
static ILB_HAL_StatusTypeDef writeSettings(void)
{
	uint16_t tmp = (settings.state | settings.integration | settings.gain);
	uint8_t payload[] = {VEML6030_ALS_CONF, (tmp & 0xFF), ((tmp >> 8) & 0xFF)};
	return (ILB_HAL_StatusTypeDef)HAL_I2C_Master_Transmit(&hi2c1, VEML6030_ADDRESS, &payload[0], 3, 200);
}

/**
 * Set the settings by index of the settings enum
 *
 * @param integrationIndex
 * @param gainIndex
 */
static void setSettingsByIndex(int8_t integrationIndex, int8_t gainIndex)
{
	// range check
	if (gainIndex > GAIN_INDEX_MAX)
	{
		gainIndex = GAIN_INDEX_MAX;
	}
	if (gainIndex < GAIN_INDEX_MIN)
	{
		gainIndex = GAIN_INDEX_MIN;
	}
	if (integrationIndex > INTEGRATION_INDEX_MAX)
	{
		integrationIndex = INTEGRATION_INDEX_MAX;
	}
	if (integrationIndex < INTEGRATION_INDEX_MIN)
	{
		integrationIndex = INTEGRATION_INDEX_MIN;
	}
	settings.gain = gainSettingsList[gainIndex];
	settings.integration = integrationTimeList[integrationIndex];
}

//
//	Implementation start
//

/**
 * Start up the sensor and automatic readout
 *
 * @return ILB_HAL_OK on success, errorcode otherwise
 */
ILB_HAL_StatusTypeDef veml6030_init(void)
{
	algorithmStatus = VEML6030_RUNNING;

	setSettingsByIndex(integrationIndex, gainIndex);
	// disable power saving mode
	uint8_t payload[] = {VEML6030_PWR_SAV, 0x00, 0x00};
	HAL_StatusTypeDef success = HAL_I2C_Master_Transmit(&hi2c1, VEML6030_ADDRESS, &payload[0], 3, 200);
	// write default settings
	success |= writeSettings();

	// trigger read out loop as soon as possible
	scheduler_registerTimerFunction(veml6030_handle, integrationTimeMS[integrationIndex] + READOUT_EXTRA_MS);

	return success;
}

/**
 * Stop regular readout of light sensor
 */
void veml6030_stop(void)
{
	algorithmStatus = VEML6030_IDLE;
}

/**
 * Main handle function. Should be called as often as possible
 *
 * This function is taken from the Vishay VEML6030 application note
 */
void veml6030_handle(void)
{
	if (algorithmStatus == VEML6030_IDLE)
	{
		// no readout. End loop
		return;
	}

	// read out the sensor
	uint8_t reading[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, VEML6030_ADDRESS, VEML6030_ALS, 1, &reading[0], 2, 200);
	// compile the reading
	uint32_t cts = (reading[1] << 8) | reading[0];

	// check if we saturate
	if (cts == 0xFFFF)
	{
		// revert back to defaults
		gainIndex = GAIN_INDEX_DEFAULT;
		integrationIndex = INTEGRATION_INDEX_DEFAULT;

		// apply settings
		changeSettings();

		// re-trigger
		scheduler_registerTimerFunction(veml6030_handle, integrationTimeMS[integrationIndex] + READOUT_EXTRA_MS);
		return;
	}

	// check if the reading is too low
	if (cts <= 100)
	{
		// first increase integration time, if we are at the limit, increase gain
		integrationIndex++;
		if (integrationIndex > INTEGRATION_INDEX_MAX)
		{
			integrationIndex = INTEGRATION_INDEX_MAX;
			// reached maximum. Increase gain
			gainIndex++;
			if (gainIndex > GAIN_INDEX_MAX)
			{
				gainIndex = GAIN_INDEX_MAX;
				// we are at the minimum. Apply value
				calculateLuxLevel(cts);
				// trigger again
				scheduler_registerTimerFunction(veml6030_handle, VEML_6030_NOMINAL_CYCLE_TIME_MS);
				return;
			}
		}

		// to have a reading, take the current one, even if it is not optimal
		calculateLuxLevel(cts);

		// apply settings and try again
		changeSettings();
		scheduler_registerTimerFunction(veml6030_handle, integrationTimeMS[integrationIndex] + READOUT_EXTRA_MS);
		return;
	}
	// large reading
	else
	{
		// are we grater than 10000 counts? if yes, decrease integration time
		if (cts > 10000)
		{
			// first decrease gain. If we have reached a minimum, decrease integration
			gainIndex--;
			if (gainIndex < GAIN_INDEX_MIN)
			{
				gainIndex = GAIN_INDEX_MIN;
				integrationIndex--;
				if (integrationIndex < INTEGRATION_INDEX_MIN)
				{
					integrationIndex = INTEGRATION_INDEX_MIN;
					// we are the minimal limit. Use valus as it is
					calculateLuxLevel(cts);
					// re-trigger;
					scheduler_registerTimerFunction(veml6030_handle, VEML_6030_MINIMAL_SETTINGS_CHANGE_TIME_MS);
					return;
				}
			}

			// get a reading if we are not saturated
			if (cts != 0xFFFF)
			{
				calculateLuxLevel(cts);
			}

			// settings were adjusted try again to read
			changeSettings();
			// re-trigger
			scheduler_registerTimerFunction(veml6030_handle, integrationTimeMS[integrationIndex] + READOUT_EXTRA_MS);
			return;
		}
		else
		{
			// in good range. Do calculation
			calculateLuxLevel(cts);
			// re-trigger
			scheduler_registerTimerFunction(veml6030_handle, VEML_6030_NOMINAL_CYCLE_TIME_MS);
		}
	}
}

/**
 * Set the currently active settings and push them to the sensor
 */
void changeSettings(void)
{
	settings.state = SensorOff;
	writeSettings();
	setSettingsByIndex(integrationIndex, gainIndex);
	settings.state = SensorOn;
	writeSettings();
	// wait at least the amount of integration time
	HAL_Delay(integrationTimeMS[integrationIndex] + 20);
}
