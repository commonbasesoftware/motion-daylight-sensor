/*
 * veml6030.h
 *
 *  Created on: Sep 22, 2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_SENSOR_VEML6030_H_
#define SRC_SENSOR_VEML6030_H_

#include <stdint.h>
#include "ilb.h"

enum VEML6030ReslutionFlagEnum {
	VEML6030_RESOLUTION_FLAG_1	= 0x00,
	VEML6030_RESOLUTION_FLAG_01	= 0x8000,
};

ILB_HAL_StatusTypeDef veml6030_init(void);
void veml6030_stop(void);

#endif /* SRC_SENSOR_VEML6030_H_ */
