/*
 * indicator.c
 *
 *  Created on: 14.10.2021
 *      Author: A.Niggebaum
 */

#include "tim.h"
#include "ilb.h"
#include "indicator.h"
#include "../Scheduler/scheduler.h"

#define INDICATOR_BASE_CLOCK_MS		40

#define BREATHE_PATTERN_MAX			20
#define BREATHE_PATTERN_LENGTH		49
static const uint8_t breathePattern[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,20,20,20,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,0,0,0,0,0};

//
//	Grab definition of pwm output timer
//
extern TIM_HandleTypeDef* pwmOutputTimer;
#define PWM_OUTPUT_MAX		(pwmOutputTimer->Instance->ARR)
#define PWM_OUTPUT_RED		(pwmOutputTimer->Instance->CCR1)
#define PWM_OUTPUT_GREEN	(pwmOutputTimer->Instance->CCR2)
#define PWM_OUTPUT_BLUE		(pwmOutputTimer->Instance->CCR3)

#define COLOR_MASK_BLUE		0x03
#define COLOR_MASK_GREEN	0x0F
#define COLOR_MASK_RED		0x03
#define COLOR_SHIFT_BLUE	0
#define COLOR_SHIFT_GREEN	2
#define COLOR_SHIFT_RED		6

#define PATTERN_PRESCALER_THRESHOLD_BLINK		5
static uint8_t patternPrescaler		  		  = 0;
#define PATTERN_PROGRESS_MAX_BLINK				8
static uint8_t patternProgress 		  		  = 0;

//
//	Private function prototypes
//
void indicator_setOutput(uint8_t colorDef);
void indicator_handle(void);;

//
//	Public variable declaration
//
uint8_t register_indicator[INDICATOR_REG_MAX] = {0};

//
//	Public function implementation
//

void indicator_init(void)
{
	// start regular call of handler
	scheduler_registerTimerFunction(indicator_handle, INDICATOR_BASE_CLOCK_MS);
}

/**
 * Handle the indicator
 */
void indicator_handle(void)
{
	// are we active (according to counter)
	if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] == INDICATOR_PATTERN_REPETITION_OFF)
	{
		// re-trigger
		scheduler_registerTimerFunction(indicator_handle, INDICATOR_BASE_CLOCK_MS);
		// exit loop
		return;
	}

	// advance prescaler
	patternPrescaler++;

	// react dependign on mode
	switch (register_indicator[INDICATOR_REG_PATTERN_DEF])
	{
	case INDICATOR_PATTERN_OFF:
		// nothing to do
		patternPrescaler = 0;
		break;

	case INDICATOR_PATTERN_BREATHE:
		// count up the pattern progress
		// apply output and scale by current position in breathe pattern
		PWM_OUTPUT_RED 		= ((PWM_OUTPUT_MAX * ((register_indicator[INDICATOR_REG_COLOR_DEFINITION] >> COLOR_SHIFT_RED) 	& COLOR_MASK_RED)/COLOR_MASK_RED) * breathePattern[patternPrescaler] / BREATHE_PATTERN_MAX);			// red
		PWM_OUTPUT_GREEN 	= ((PWM_OUTPUT_MAX * ((register_indicator[INDICATOR_REG_COLOR_DEFINITION] >> COLOR_SHIFT_GREEN) & COLOR_MASK_GREEN)/COLOR_MASK_GREEN) * breathePattern[patternPrescaler] / BREATHE_PATTERN_MAX);		// green
		PWM_OUTPUT_BLUE 	= ((PWM_OUTPUT_MAX * ((register_indicator[INDICATOR_REG_COLOR_DEFINITION] >> COLOR_SHIFT_BLUE)  & COLOR_MASK_BLUE)/COLOR_MASK_BLUE) * breathePattern[patternPrescaler] / BREATHE_PATTERN_MAX);		// blue

		if (patternPrescaler >= BREATHE_PATTERN_LENGTH)
		{
			patternPrescaler = 0;

			// advance counter
			if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] > INDICATOR_PATTERN_REPETITION_OFF &&
				register_indicator[INDICATOR_REG_REPETITION_COUNTER] != INDICATOR_PATTERN_REPETITION_INFINITE)
			{
				register_indicator[INDICATOR_REG_REPETITION_COUNTER]--;
			}
		}
		break;

	case INDICATOR_PATTERN_SIMPLE_BLINK:
		if (patternPrescaler >= PATTERN_PRESCALER_THRESHOLD_BLINK)
		{
			// apply output
			if (((register_indicator[INDICATOR_REG_SIMPLE_PATTERN_DEFINITION] >> patternProgress) & 0x01) == 0x01)
			{
				indicator_setOutput(register_indicator[INDICATOR_REG_COLOR_DEFINITION]);
			} else {
				indicator_setOutput(0x00);
			}

			patternProgress++;
			if (patternProgress >= PATTERN_PROGRESS_MAX_BLINK)
			{
				patternProgress = 0;

				// advance counter
				if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] > INDICATOR_PATTERN_REPETITION_OFF &&
						register_indicator[INDICATOR_REG_REPETITION_COUNTER] != INDICATOR_PATTERN_REPETITION_INFINITE)
				{
					register_indicator[INDICATOR_REG_REPETITION_COUNTER]--;
					// make sure the LED is off
					if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] == 0)
					{
						indicator_setOutput(0x00);
					}
				}
			}
			patternPrescaler = 0;
		}
		break;

	case INDICATOR_PATTERN_COLOR_BLINK:
		if (patternPrescaler >= PATTERN_PRESCALER_THRESHOLD_BLINK)
		{
			// apply output
			indicator_setOutput(register_indicator[INDICATOR_REG_MULTI_COLOR_PATTERN_0 + patternProgress]);

			patternProgress++;
			if (patternProgress >= PATTERN_PROGRESS_MAX_BLINK)
			{
				patternProgress = 0;

				// advance counter
				if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] > INDICATOR_PATTERN_REPETITION_OFF &&
						register_indicator[INDICATOR_REG_REPETITION_COUNTER] != INDICATOR_PATTERN_REPETITION_INFINITE)
				{
					register_indicator[INDICATOR_REG_REPETITION_COUNTER]--;
					// make sure the LED is off
					if (register_indicator[INDICATOR_REG_REPETITION_COUNTER] == 0)
					{
						indicator_setOutput(0x00);
					}
				}
			}
			patternPrescaler = 0;
		}
		break;
	}

	// trigger again
	scheduler_registerTimerFunction(indicator_handle, INDICATOR_BASE_CLOCK_MS);
}

/**
 * Must be called after content of the register has changed.
 */
void indicator_applyChanges(void)
{
	switch (register_indicator[INDICATOR_REG_PATTERN_DEF])
	{
	case INDICATOR_PATTERN_OFF:
		// disable all output
		indicator_setOutput(0x00);
		break;

	case INDICATOR_PATTERN_BREATHE:
		// start breathing
		break;

	case INDICATOR_PATTERN_SIMPLE_BLINK:
		// start simple pattern
		break;

	case INDICATOR_PATTERN_COLOR_BLINK:
		// start color blink
		break;

	default:
		// unsupported pattern/behaviour definition
		break;
	}
	// reset progress counter
	patternProgress = 0;
	patternPrescaler = 0;
}

//
//	Private function implementations
//
void indicator_setOutput(uint8_t colorDef)
{
	// push the color settings to the indicator
	PWM_OUTPUT_RED 		= (PWM_OUTPUT_MAX * ((colorDef >> COLOR_SHIFT_RED) 	 & COLOR_MASK_RED)/COLOR_MASK_RED);			// red
	PWM_OUTPUT_GREEN 	= (PWM_OUTPUT_MAX * ((colorDef >> COLOR_SHIFT_GREEN) & COLOR_MASK_GREEN)/COLOR_MASK_GREEN);		// green
	PWM_OUTPUT_BLUE 	= (PWM_OUTPUT_MAX * ((colorDef >> COLOR_SHIFT_BLUE)  & COLOR_MASK_BLUE)/COLOR_MASK_BLUE);		// blue
}
