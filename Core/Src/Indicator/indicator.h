/*
 * indicator.h
 *
 *  Created on: 14.10.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_INDICATOR_INDICATOR_H_
#define SRC_INDICATOR_INDICATOR_H_

//
//	Register content and setting enumerations
//
// defines for indicator register
enum IndicatorRegisterEnum {
	INDICATOR_REG_PATTERN_DEF,
	INDICATOR_REG_COLOR_DEFINITION,
	INDICATOR_REG_SIMPLE_PATTERN_DEFINITION,
	INDICATOR_REG_REPETITION_COUNTER,
	INDICATOR_REG_MULTI_COLOR_PATTERN_0,
	INDICATOR_REG_MULTI_COLOR_PATTERN_1,
	INDICATOR_REG_MULTI_COLOR_PATTERN_2,
	INDICATOR_REG_MULTI_COLOR_PATTERN_3,
	INDICATOR_REG_MULTI_COLOR_PATTERN_4,
	INDICATOR_REG_MULTI_COLOR_PATTERN_5,
	INDICATOR_REG_MULTI_COLOR_PATTERN_6,
	INDICATOR_REG_MULTI_COLOR_PATTERN_7,
	INDICATOR_REG_MAX
};

enum IndicatorPatternDefEnum {
	INDICATOR_PATTERN_OFF 			= 0x00,
	INDICATOR_PATTERN_BREATHE 		= 0x01,
	INDICATOR_PATTERN_SIMPLE_BLINK	= 0x02,
	INDICATOR_PATTERN_COLOR_BLINK	= 0x04
};

enum IndicatorPatternRepetitionEnum {
	INDICATOR_PATTERN_REPETITION_OFF 		= 0x00,
	INDICATOR_PATTERN_REPETITION_INFINITE 	= 0xFF
};

//
//	Interface for register
//
// register array. To be managed by register routines
extern uint8_t register_indicator[INDICATOR_REG_MAX];
void indicator_applyChanges(void);
void indicator_init(void);

#endif /* SRC_INDICATOR_INDICATOR_H_ */
