/*
 * memory.c
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 *
 * This file bundles all functions and definitions in connection with memory access.
 * This includes writing and reading tables and lookup structures.
 */

#include "main.h"
#include "stm32l0xx_hal.h"
#include "ilb.h"

//
//	memory mapping.
//
#define EEPROM_RESET_FLAG_START			DATA_EEPROM_BASE						//!< position of the first byte flag. There are four flags in total
#define EEPROM_RESET_FLAG_VALUE			0xFF									//!< flag value to be set
#define EEPROM_RESET_FLAG_MAX_NUMBER	4										//!< number of reset flags in bytes
#define EEPROM_RESET_FLAG_END			(EEPROM_RESET_FLAG_START + EEPROM_RESET_FLAG_MAX_NUMBER)

// address lookup table
//#define EEPROM_NOF_PERIPHERALS			(DATA_EEPROM_BASE + 8)					//!< starting address of word indicating the number of stored entries in the peripheral table.
//#define EEPROM_TABLE_START_ADDRESS		(EEPROM_NOF_PERIPHERALS + 1)    		//!< starting address of peripheral table storage block
//#define CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS		5							//!< defines the number of bytes that each element of ::corePeripheralEntryStruct takes in the stored eeprom
//#define MAX_SUPPORTED_NUMBER_OF_PERIPHERALS			20

// pwm and light engine configuration
#define EEPROM_PWM_SETTINGS_STRUCT_START		(EEPROM_RESET_FLAG_END + 1)			//!< start of eeprom lookup
#define EEPROM_PWM_SETTINGS_MAX_SIZE			(10 + 0xFF)						//!< maximal size of eepom lookup part. 10 bytes for header and 255 bytes for mired lookup
#define EEPROM_PWM_SETTINGS_STRUCT_END			(EEPROM_PWM_SETTINGS_STRUCT_START + EEPROM_PWM_SETTINGS_MAX_SIZE)

// the telemetry variables need to be aligned for pointer to words (32bit)
#define EEPROM_TELEMETRY_START					(EEPROM_PWM_SETTINGS_STRUCT_END + 1 + (4 - ((EEPROM_PWM_SETTINGS_STRUCT_END + 1) % 4)))
#define EEPROM_TELEMETRY_END					(EEPROM_TELEMETRY_START + 8)

#define EEPROM_ADDRESS_START					(EEPROM_TELEMETRY_END)
#define EEPROM_ADDRESS_END						(EEPROM_ADDRESS_START + 1)

#define EEPROM_LOGIC_START						(EEPROM_ADDRESS_END)

#define EEPROM_LOGIC_ERASE_CONSECUTIVE_ZERO_THRESHOLD	30

// internal variables
static uint8_t numberOfResetFlags = 0;
//static uint8_t numberOfEntriesInAddressTable = 0;

//
//	Function prototypes
//
static uint8_t unlockEEPROM(void);

/**
 * Initialisation routine for memory. Pushes important information from the memory into temporary variables
 */
void memory_init(void)
{
	// unlock writing to eeprom
	unlockEEPROM();

	// check how many reset flags are present
	numberOfResetFlags = 0;
	while (numberOfResetFlags < EEPROM_RESET_FLAG_MAX_NUMBER)
	{
		if (*(uint8_t*)(EEPROM_RESET_FLAG_START+numberOfResetFlags) == EEPROM_RESET_FLAG_VALUE) {
			// flag found
			numberOfResetFlags++;
		} else {
			// no flag found
			break;
		}
	}
}

//
//	SECTION: saving and loading of PWM output structure
//
/**
 * Store the light engine settings to memory
 *
 * @param settings
 * @return
 */
ILB_HAL_StatusTypeDef memory_writePWMLookupStructure(LightEngineSettings_t* settings)
{
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START) 	  = settings->version;
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 1) = settings->Limit_CHA;
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 2) = settings->Limit_CHB;
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 3) = settings->channelMapping;
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 4) = settings->followerGroup;
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 5) = ((settings->miredMin >> 8) & 0xFF);
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 6) = (settings->miredMin & 0xFF);
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 7) = ((settings->miredMax >> 8) & 0xFF);
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 8) = (settings->miredMax & 0xFF);
	*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 9) = settings->miredStep;

	// calculate size of mired lookup
	uint8_t miredTableSize = ((settings->miredMax - settings->miredMin)/settings->miredStep) + 1;
	for (uint8_t i=0; i<miredTableSize; i++)
	{
		*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 11 + i) = settings->miredLookup[i];
	}

	return ILB_HAL_OK;
}

/**
 * Read the pwm/mired/cct lookup table structure from memory
 *
 * @param target	Structure to read the values into
 * @param version
 * @return
 */
ILB_HAL_StatusTypeDef memory_readPWMLookupStructure(LightEngineSettings_t* target, uint8_t version)
{
	// check version
	if (*(uint8_t*)EEPROM_PWM_SETTINGS_STRUCT_START != version)
	{
		return ILB_HAL_ERROR;
	}

	// read the information from the memory
	target->version 		= *(uint8_t*)EEPROM_PWM_SETTINGS_STRUCT_START;
	target->Limit_CHA 		= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 1);
	target->Limit_CHB 		= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 2);
	target->channelMapping 	= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 3);
	target->followerGroup   = *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 4);
	target->miredMin 		= (*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 5) << 8);
	target->miredMin	   |= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 6);
	target->miredMax 		= (*(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 7) << 8);
	target->miredMax	   |= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 8);
	target->miredStep		= *(uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 9);

	// fill the lookup table
	target->miredLookup 	= (uint8_t*)(EEPROM_PWM_SETTINGS_STRUCT_START + 11);

	return ILB_HAL_OK;
}

//
//	SECTION: all things power cycle flag tracking
//
/**
 * Add a power cycle flag
 */
void memory_addPowerCycleFlag(void)
{
	*(uint8_t *)(EEPROM_RESET_FLAG_START + numberOfResetFlags) = EEPROM_RESET_FLAG_VALUE;
	numberOfResetFlags++;
}

/**
 * Clear the power cycle flags
 */
void memory_clearPowerCycleFlags(void)
{
	*(volatile uint32_t *)(EEPROM_RESET_FLAG_START) = (uint32_t)0;
	__WFI();
	numberOfResetFlags = 0;
}

/**
 * Accessor for number of resets flags present in memory
 * @return
 */
uint8_t memory_getNumberOfResetFlags(void)
{
	return numberOfResetFlags;
}

////
////	SECTION: persistent variables
////
//// contains duds for now
//// the number of persistent (32bit) variables is defined as ILB_
//ILB_HAL_StatusTypeDef ILB_HAL_writePersistentVariable(uint8_t index, uint32_t* value)
//{
//	return ILB_HAL_OK;
//}
//
//ILB_HAL_StatusTypeDef ILB_HAL_getPersistentVariable(uint8_t index, uint32_t* value)
//{
//	*value = 0x00;
//	return ILB_HAL_OK;
//}

//
//	Storing and retreival of telemetry
//
ILB_HAL_StatusTypeDef ILB_HAL_storeTelemetry(uint32_t onTime, uint32_t burnTime)
{
	*(uint32_t*)EEPROM_TELEMETRY_START = onTime;
	*(uint32_t*)(EEPROM_TELEMETRY_START + 4) = burnTime;
	return ILB_HAL_OK;
}

ILB_HAL_StatusTypeDef ILB_HAL_getTelemetry(uint32_t* onTime, uint32_t* burnTime)
{
	*onTime 	= *(uint32_t*)(EEPROM_TELEMETRY_START);
	*burnTime 	= *(uint32_t*)(EEPROM_TELEMETRY_START + 4);
	return ILB_HAL_OK;
}

//
//	Storing and access to address byte
//
ILB_HAL_StatusTypeDef ILB_HAL_getAddress(uint8_t* address)
{
	*address = *(uint8_t*)EEPROM_ADDRESS_START;
	return ILB_HAL_OK;
}

ILB_HAL_StatusTypeDef ILB_HAL_storeAddress(uint8_t* address)
{
	*(uint8_t*)EEPROM_ADDRESS_START = *address;
	return ILB_HAL_OK;
}

//
//	Access for logic to eeprom
//
ILB_HAL_StatusTypeDef memory_EEPROMInterface(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar)
{
	// first, check if we need to write or read
	if ((accessor & EEPROM_VAR_WRITE) == EEPROM_VAR_WRITE)
	{
		if ((accessor & EEPROM_VAR_BYTE) == EEPROM_VAR_BYTE)
		{
			*(uint8_t*)(EEPROM_LOGIC_START + offsetBytes) = (uint8_t)(*pVar & 0xFF);
			return ILB_HAL_OK;
		}
		else if ((accessor & EEPROM_VAR_HALF_WORD) == EEPROM_VAR_HALF_WORD)
		{
			*(uint16_t*)(EEPROM_LOGIC_START + offsetBytes) = (uint16_t)(*pVar & 0xFFFF);
			return ILB_HAL_OK;
		}
		else if ((accessor & EEPROM_VAR_WORD) == EEPROM_VAR_WORD)
		{
			*(uint32_t*)(EEPROM_LOGIC_START + offsetBytes) = *pVar;
			return ILB_HAL_OK;
		}
	}
	// read section
	else
	{
		if ((accessor & EEPROM_VAR_BYTE) == EEPROM_VAR_BYTE)
		{
			// we want to read a byte
			*pVar = (uint32_t)(*(uint8_t*)(EEPROM_LOGIC_START + offsetBytes));
			return ILB_HAL_OK;
		}
		else if ((accessor & EEPROM_VAR_HALF_WORD) == EEPROM_VAR_HALF_WORD)
		{
			*pVar = (uint32_t)(*(uint16_t*)(EEPROM_LOGIC_START+ offsetBytes));
			return ILB_HAL_OK;
		}
		else if ((accessor & EEPROM_VAR_WORD) == EEPROM_VAR_WORD)
		{
			*pVar = (uint32_t)(*(uint32_t*)(EEPROM_LOGIC_START + offsetBytes));
			return ILB_HAL_OK;
		}
	}
	return ILB_HAL_ERROR;
}

/**
 * Erase all memory accessed by the logic
 * @return
 */
ILB_HAL_StatusTypeDef memory_eraseLogicEEPROM(void)
{
	uint8_t counter = 0;
	uint32_t offset = 0;
	while (counter < EEPROM_LOGIC_ERASE_CONSECUTIVE_ZERO_THRESHOLD)
	{
		// counter up criterion
		if (*(uint8_t*)(EEPROM_LOGIC_START + offset) == 0)
		{
			counter++;
		} else {
			counter = 0;
		}
		*(uint8_t*)(EEPROM_LOGIC_START + offset++) = 0x00;
	}
	return ILB_HAL_OK;
}

//
//	SECTION: internal helper functions
//
/**
 * Unlock the eeprom memory
 *
 * @return 0 on success 1 otherwise
 */
uint8_t unlockEEPROM(void)
{
	uint8_t timeoutTarget = HAL_GetTick() + 5;		// 5ms timeout for unlocking the eeprom
	uint8_t wrap = 0;
	if (timeoutTarget < HAL_GetTick()) {
		wrap = 1;
	}

	// wait until all operations have finished
	while ((FLASH->SR & FLASH_SR_BSY) != 0) {
		if (HAL_GetTick() > timeoutTarget) {
			if(wrap == 1) {
				wrap = 0;
			} else {
				return 1;
			}
		}
	}
	if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0) {
		FLASH->PEKEYR = FLASH_PEKEY1;
		FLASH->PEKEYR = FLASH_PEKEY2;
		return 0;
	}
	return 1;
}
