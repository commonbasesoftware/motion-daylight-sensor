/*
 * hwInterface.c
 *
 *  Created on: 13.10.2021
 *      Author: A.Niggebaum
 */

#include "main.h"
#include "tim.h"
#include "memory.h"
#include "messageHandling.h"
#include "../Sensors/PYQ_1548.h"
#include "../Sensors/veml6030.h"
#include "ilb.h"
#include "../HWFunctions/button.h"
#include "../Scheduler/scheduler.h"

/*
 * Timer definitions
 */
TIM_HandleTypeDef* schedulerTimer = &htim6;			//!< 5ms timer for general handling of tasks and timeouts
TIM_HandleTypeDef* pwmOutputTimer = &htim2;			//!< PWM generating timer for outputs
TIM_HandleTypeDef* slow1sTimer	  = &htim22;				//!< timer running at 1s interval for long periods

/*
 * Status variables for button tracking
 */
#define BUTTON_MASK				0x0FF0L													//!< mask of don't care bytes
#define BUTTON_PRESSED_MASK		0x000FL													//!< pattern for button pressed after mask is applied
#define BUTTON_RELEASED_MASK	0xF000L													//!< pattern for button released after mask is applied
#define BUTTON_COOLDOWN_TIME_MS	100														//!< time before a second button press is detected
static uint16_t buttonInput = 0;
typedef enum buttonPressEnum {
	BUTTON_NONE,																		//!< no button event pending
	BUTTON_COOLDOWN,																	//!< cooldown phase
	BUTTON_RELEASED,																	//!< button release devent pending
	BUTTON_PRESSED																		//!< button pressed event pending
} buttonPressTypeDef;
static buttonPressTypeDef buttonPressed = BUTTON_NONE;											//!< button status variable
static uint32_t buttonPressedTick = 0;															//!< system counter value when button was pressed
static void button_arm(void);

/**
 * General hardware initialisation function
 *
 * This function is called from the base library
 *
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_hardwareInit(void)
{
	// intialise memory interface
	memory_init();

	// start generation of PWM outputs for RGB led
	HAL_TIM_PWM_Start(pwmOutputTimer, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(pwmOutputTimer, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(pwmOutputTimer, TIM_CHANNEL_3);

	// start timer for scheduler
	HAL_TIM_Base_Start_IT(schedulerTimer);
	// start the slow timer
	HAL_TIM_Base_Start_IT(slow1sTimer);

	// initialise interface to light sensor
	veml6030_init();

	// start the motion sensor
	PYQ1548_init();

	// start message receival
	ILBStartMessageReceival();

	return ILB_HAL_OK;
}

//
//	Hardware information
//
uint32_t ILB_HAL_getUID(void)
{
	return (HAL_GetUIDw0() ^ HAL_GetUIDw1() ^ HAL_GetUIDw2());
}

//
//	UART communication
//
/**
 * Send bytes via the UART interface
 *
 * This routines is called from the library if it wants to send bytes. This function must be non blocking.
 * @param buffer
 * @param nOfBytes
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_UARTSendBytes(uint8_t* buffer, uint16_t nOfBytes)
{
	return ILBSendMessage(buffer, nOfBytes);
}

/**
 * Accessor for library if lines is busy.
 *
 * @return 0 if line is free
 */
uint8_t ILB_HAL_isLineBusy(void)
{
	return messageHandling_isBusBusy();
}

/**
 * Accessor to tick counter
 * @return
 */
uint32_t ILB_HAL_getTick(void)
{
	return HAL_GetTick();
}

/**
 * Implementation of ST HAL timer callback
 * @param htim
 */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{
  if (htim == schedulerTimer) {
    // Scheduler timer has expired, set flag
	  lib_setSchedulerStatusWaiting();
	  // also use this timer to detect button
	  buttonInput = buttonInput << 1;
	  if (HAL_GPIO_ReadPin(BUTTON_IN_GPIO_Port, BUTTON_IN_Pin) == GPIO_PIN_RESET) {
		  buttonInput |= 0x01;
	  }
	  // check if this is a trigger. Don't react if we are in the cooldown phase
	  if (buttonPressed == BUTTON_NONE)
	  {
		  // button pressed
		  if ((buttonInput | BUTTON_MASK) == (BUTTON_PRESSED_MASK | BUTTON_MASK))
		  {
			  buttonPressedTick = HAL_GetTick();
		  }
		  // button released
		  if ((buttonInput | BUTTON_MASK) == (BUTTON_RELEASED_MASK | BUTTON_MASK))
		  {
			  // calculate hold time in units of 100ms
			  buttonPressedTick = (HAL_GetTick() - buttonPressedTick)/100 + 1;
			  buttonPressed = BUTTON_RELEASED;
		  }
	  }
  } else if (htim == slow1sTimer) {
	  lib_arm1sTimer();
  } else {
	  // forward to device
	  //ilbTimerCallback(htim);
  }
}

/**
 * Handler function for button state
 *
 * @param holdTime
 * @return
 */
void button_getHoldTime(uint16_t* holdTime)
{
	if (buttonPressed == BUTTON_RELEASED)
	{
		// fill value
		*holdTime = (buttonPressedTick & 0xFFFF);
		// mark as cool down
		buttonPressed = BUTTON_COOLDOWN;
		// start timer
		scheduler_registerTimerFunction(button_arm, BUTTON_COOLDOWN_TIME_MS);
	} else {
		*holdTime = BUTTON_IDLE;
	}
}

/**
 * Arm the button again
 */
void button_arm(void)
{
	buttonPressed = BUTTON_NONE;
}
