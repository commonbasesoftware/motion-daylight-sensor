/*
 * button.h
 *
 *  Created on: Oct 19, 2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_HWFUNCTIONS_BUTTON_H_
#define SRC_HWFUNCTIONS_BUTTON_H_

#define BUTTON_IDLE			0x00
void button_getHoldTime(uint16_t* holdTime);

#endif /* SRC_HWFUNCTIONS_BUTTON_H_ */
