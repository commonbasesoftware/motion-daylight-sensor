/*
 * memory.h
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_INTERFACE_MEMORY_H_
#define SRC_INTERFACE_MEMORY_H_

#include <stdint.h>
#include "ilb.h"

void memory_init(void);
void memory_addPowerCycleFlag(void);
void memory_clearPowerCycleFlags(void);
uint8_t memory_getNumberOfResetFlags(void);

ILB_HAL_StatusTypeDef memory_writePWMLookupStructure(LightEngineSettings_t* settings);
ILB_HAL_StatusTypeDef memory_readPWMLookupStructure(LightEngineSettings_t* target, uint8_t version);

ILB_HAL_StatusTypeDef memory_EEPROMInterface(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar);
ILB_HAL_StatusTypeDef memory_eraseLogicEEPROM(void);

#endif /* SRC_INTERFACE_MEMORY_H_ */
