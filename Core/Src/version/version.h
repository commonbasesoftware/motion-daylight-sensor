/*
 * version.h
 *
 *  Created on: 14.10.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_VERSION_VERSION_H_
#define SRC_VERSION_VERSION_H_

#define SENSOR_SW_VERSION_MAJOR		0
#define SENSOR_SW_VERSION_MINOR		2

#endif /* SRC_VERSION_VERSION_H_ */
