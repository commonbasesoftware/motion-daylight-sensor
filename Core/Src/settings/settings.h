/*
 * settings.h
 *
 *  Created on: 14.10.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_SETTINGS_SETTINGS_H_
#define SRC_SETTINGS_SETTINGS_H_

#define SETTING_REPORT_TIMEOUT_MS			500				//!< timeout for a report (motion, daylight etc.)

#endif /* SRC_SETTINGS_SETTINGS_H_ */
