/*
 * reset.c
 *
 *  Created on: 15.10.2021
 *      Author: A.Niggebaum
 */

#include "../HWFunctions/memory.h"

void eraseSettings(void)
{
	// actually use the erase logic function that will zero all custom settings
	memory_eraseLogicEEPROM();
}

extern void lib_executeFactoryReset(void)
{
	eraseSettings();
}

extern void lib_executeSettingsReset(void)
{
	eraseSettings();
}
