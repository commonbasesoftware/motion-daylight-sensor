/*
 * register.c
 *
 *  Created on: Oct 13, 2021
 *      Author: A.Niggebaum
 *
 * This file handles reading from and writing to registers
 *
 * Note that some registers have a lookup table with triggers. They are a list of handlers that
 * should be called after the value in the register has been changed. This should be used to e.g. trigger
 * a push of settings to a sensor. Also note that if consecutive handler are the same, they should only be
 * called if the handler changes in consecutive writes. For example, it does not make sense to push settings to
 * the motion sensor after each individual register has been written but only after the block of settings has
 * been finished.
 *
 */

#include <stdint.h>
#include <string.h>
#include "ilb.h"
#include "tim.h"
#include "register.h"
#include "../version/version.h"
#include "../Messaging/messaging.h"
#include "../Indicator/indicator.h"
#include "../Sensors/PYQ_1548.h"
#include "../HWFunctions/memoryLocations.h"
#include "../HWFunctions/memory.h"


//
//	private functions
//
void writeSettingsToMemory(void);

//
//	Prototypes and register change handler
//
typedef void(*registerChangeHandler_t)(void);

static registerChangeHandler_t SettingsChangeHandler[] = {
		NULL,
		NULL,
		NULL,
		NULL,
		writeSettingsToMemory,
		PYQ1548_updateFromRegister,
		PYQ1548_updateFromRegister,
		PYQ1548_updateFromRegister,
		NULL,
		writeSettingsToMemory,
		writeSettingsToMemory,
		writeSettingsToMemory,
		writeSettingsToMemory,
};

// List of handler that should be called after registers have been written to the indicator register
static registerChangeHandler_t IndicatorChangeHandler[] = {
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
		indicator_applyChanges,
};

//
//	Register arrays
//
uint8_t registerSettings[REGISTER_SETTINGS_MAX] = {0};
uint8_t registerReadings[REGISTER_READING_MAX] = {0};

/**
 * Initialise registers
 */
void registerSensor_init(void)
{
	// fill with constants
	registerSettings[REGISTER_SETTINGS_SOFTWARE_MAJOR] = SENSOR_SW_VERSION_MAJOR;
	registerSettings[REGISTER_SETTINGS_SOFTWARE_MINOR] = SENSOR_SW_VERSION_MINOR;
	uint32_t tmp = 0;
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_REPORTING, &tmp);
	registerSettings[REGISTER_SETTINGS_REPORTING] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_MSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_MSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_LSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_LSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_MSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_LSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB] = (tmp & 0xFF);

	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_MSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_MSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_LSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_LSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_MSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB] = (tmp & 0xFF);
	memory_EEPROMInterface(EEPROM_VAR_BYTE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_LSB, &tmp);
	registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB] = (tmp & 0xFF);

	// catch uninitialised case
	if (registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB] == 0 && registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB] == 0)
	{
		registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB] = 0xFF;
		registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB] = 0xFF;
	}
}

/**
 * Read from the register
 *
 * @param message
 */
void registerSensor_read(ILBMessageStruct* message)
{
	// check integrity of command
	if (message->payloadSize != 3)
	{
		messaging_respondErrorMessage(message, ILB_ERROR_INVALID);
		return;
	}

	// extract bounds
	uint8_t startIndex = message->payload[1];
	uint8_t nOfBytes = message->payload[2];

	ILBMessageStruct response;
	response.address = message->address;
	response.identifier = message->identifier;
	response.messageType = ILB_MESSAGE_ACHKNOWLEDGE;
	response.payloadSize = nOfBytes;

	// select register
	switch (message->payload[0])
	{
	case REGISTER_SETTINGS:
		if (startIndex + nOfBytes > REGISTER_SETTINGS_MAX)
		{
			messaging_respondErrorMessage(message, ILB_ERROR_OUT_OF_BOUNDS);
			return;
		}
		// copy content to response
		memcpy(&response.payload[0], &registerSettings[startIndex], nOfBytes);
		break;

	case REGISTER_READINGS:
		if (startIndex + nOfBytes > REGISTER_READING_MAX)
		{
			messaging_respondErrorMessage(message, ILB_ERROR_OUT_OF_BOUNDS);
			return;
		}
		// copy content to response
		memcpy(&response.payload[0], &registerReadings[startIndex], nOfBytes);
		// clear first register if it was read
		if (startIndex <= REGISTER_READING_MOTION_DAYLIGHT_STATUS && startIndex + nOfBytes >= REGISTER_READING_MOTION_DAYLIGHT_STATUS)
		{
			registerReadings[REGISTER_READING_MOTION_DAYLIGHT_STATUS] = 0x00;
		}
		break;

	default:
		messaging_respondErrorMessage(message, ILB_ERROR_OUT_OF_BOUNDS);
		return;
	}

	// pad with zeros in case the message is too short
	while (response.payloadSize < 2)
	{
		response.payload[response.payloadSize++] = 0x00;
	}

	// send answer
	messaging_sendMessage(&response, NULL, 0, NULL);
}

/**
 * Write handling of register
 *
 * @param message
 */
void registerSensor_write(ILBMessageStruct* message)
{
	// extract parameter
	uint8_t startIndex = message->payload[1];
	uint8_t nOfBytes = message->payloadSize - 2;
	uint8_t index = 0;

	registerChangeHandler_t* listOfChangeHandler;
	uint8_t* registerLocation;

	switch (message->payload[0])
	{
	case REGISTER_SETTINGS:
		if (startIndex < REGISTER_SETTINGS_REPORTING  || startIndex + nOfBytes > REGISTER_SETTINGS_MAX)
		{
			messaging_respondErrorMessage(message, ILB_ERROR_OUT_OF_BOUNDS);
			return;
		}
		registerLocation = &registerSettings[0];
		listOfChangeHandler = &SettingsChangeHandler[0];
		break;

	case REGISTER_INDICATOR:
		if (startIndex + nOfBytes > INDICATOR_REG_MAX)
		{
			messaging_respondErrorMessage(message, ILB_ERROR_OUT_OF_BOUNDS);
			return;
		}
		registerLocation = &register_indicator[0];
		listOfChangeHandler = &IndicatorChangeHandler[0];
		break;

	default:
		return;
	}

	// apply the changes
	registerChangeHandler_t lastUpdateHandler = listOfChangeHandler[startIndex];
	while (startIndex + index < startIndex + nOfBytes)
	{
		// apply changes
		registerLocation[startIndex + index] = message->payload[2 + index];
		// check if the change needs to be handled
		if (listOfChangeHandler[startIndex+index] != NULL && lastUpdateHandler == listOfChangeHandler[startIndex+index])
		{
			// check if it is the same handler. If so, we don't need to execute (yet). Bytes might be missing
		} else if (listOfChangeHandler[startIndex+index] != NULL && lastUpdateHandler != listOfChangeHandler[startIndex+index])
		{
			// execute change handler
			listOfChangeHandler[startIndex+index]();
			// store for next loop
			lastUpdateHandler = listOfChangeHandler[startIndex+index];
		}
		index++;
	}
	// execute handler in any case
	listOfChangeHandler[startIndex+index-1]();
	messaging_respondErrorMessage(message, ILB_OK);
}

/**
 * Write the reporting flag to memory
 */
void writeSettingsToMemory(void)
{
	uint32_t tmp = registerSettings[REGISTER_SETTINGS_REPORTING];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_REPORTING, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_MSB];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_MSB, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_LSB];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_THRESHOLD_LOW_LSB, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_MSB, &tmp);
	tmp = registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB];
	memory_EEPROMInterface(EEPROM_VAR_BYTE | EEPROM_VAR_WRITE, SENSOR_MEMORY_SETTINGS_THRESHOLD_HIGH_LSB, &tmp);
}
