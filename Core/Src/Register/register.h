/*
 * register.h
 *
 *  Created on: Oct 13, 2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_REGISTER_REGISTER_H_
#define SRC_REGISTER_REGISTER_H_

#include "ilb.h"

typedef enum RegisterIndexEnum {
	REGISTER_SETTINGS = 0x02,
	REGISTER_READINGS = 0x03,
	REGISTER_INDICATOR = 0x04,
} RegisterIndex_t;

enum SettingsRegisterEnum {
	REGISTER_SETTINGS_SOFTWARE_MAJOR,					//!< contains the major software version of the sensor
	REGISTER_SETTINGS_SOFTWARE_MINOR,					//!< contains the minor software version of the sensor
	REGISTER_SETTINGS_PRODUCTION_DATE_MSB,				//!< production date of hardware (currently not used)
	REGISTER_SETTINGS_PRODUCTION_DATE_LSB,				//!< production date of hardware (currently not used)
	REGISTER_SETTINGS_REPORTING,						//!< reporting settings
	REGISTER_SETTINGS_MOTION_SENSITIVITY,				//!< sensitivity settings for motion sensor
	REGISTER_SETTINGS_SENSOR_SETTINGS_1,				//!< settings set 1 for motion sensor
	REGISTER_SETTINGS_SENSOR_SETTINGS_2,				//!< settings set 2 for motion sensor
	REGISTER_SETTINGS_DAYLIGHT_SETTINGS,				//!< settings for daylight sensor
	REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_MSB,		//!< msb for daylight level threshold lower limit
	REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_LSB,		//!< lsb for daylight level threshold lower limit
	REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB,		//!< msb for daylight level threshold upper limit
	REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB,		//!< msb for daylight level threshold upper limit
	REGISTER_SETTINGS_MAX,
};

enum SettingsRegisterReportingEnum {
	REGISTER_SETTINGS_REPORT_MOTION = 0x01,
	REGISTER_SETTINGS_REPORT_DAYIGHT_THRESHOLD_CROSSING = 0x02,
};

enum ReadingRegisterEnum {
	REGISTER_READING_MOTION_DAYLIGHT_STATUS,
	REGISTER_READING_DAYLIGHT_FILTERED_MSB,
	REGISTER_READING_DAYLIGHT_FILTERED_LSB,
	REGISTER_READING_DAYLIGHT_UNIFLTERED_MSB,
	REGISTER_READING_DAYLIGHT_UNFILTERED_LSB,
	REGISTER_READING_MAX
};

enum ReadingRegisterEventEnum {
	REGISTER_READING_MOTION_EVENT 		= 0x01,
	REGISTER_READING_DAYLIGHT_TO_LOW	= 0x02,
};

//
//	Register arrays
//
extern uint8_t registerSettings[REGISTER_SETTINGS_MAX];
extern uint8_t registerReadings[REGISTER_READING_MAX];

//
//	Interface functions
//
void registerSensor_init(void);
void registerSensor_read(ILBMessageStruct* message);
void registerSensor_write(ILBMessageStruct* message);

#endif /* SRC_REGISTER_REGISTER_H_ */
