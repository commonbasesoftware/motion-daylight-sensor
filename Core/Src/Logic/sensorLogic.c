/*
 * logic.c
 *
 *  Created on: Oct 12, 2021
 *      Author: A.Niggebaum
 */

#include "main.h"
#include "tim.h"

#include "ilb.h"
#include "ilbstd.h"
#include "messageHandling.h"
#include "../Register/register.h"
#include "../Sensors/PYQ_1548.h"
#include "../Sensors/veml6030.h"
#include "../Messaging/messaging.h"
#include "../Indicator/indicator.h"
#include "../settings/settings.h"
#include "../HWFunctions/button.h"

// extern constants required by library
uint8_t const libLogic_deviceClass = ILB_PERIPHERAL_MOTION_DAYLIGHT_CLASS;


//
//	Private function prototypes
//
void reportCallback(ILBMessageStruct* message);
void reportTimeout(void);

//
//	State management of sensor
//
typedef enum SensorStateEnum {
	SENSOR_STATE_INIT,					//!< initialisation state. After success, automatic progression to ::SENSOR_STATE_WAITING_FOR_START
	SENSOR_STATE_WAITING_FOR_START,		//!< waiting for start operation. Disables all eventual reporting
	SENSOR_STATE_RUNNING,				//!< normal operation. Waiting for events
	SENSOR_STATE_REPORTING,				//!< report has been sent out and we are waiting for an acknowledge or timeout
	SENSOR_STATE_ERROR					//!< error occured.
} SensorState_t;
static SensorState_t sensorState = SENSOR_STATE_INIT;
static uint8_t startOperationReceived = 0;

//
//	Button handling
//
static uint16_t buttonHoldTime = 0;

/**
 * Main sensor logic routine
 */
void lib_logic(void)
{
	switch (sensorState)
	{
	case SENSOR_STATE_INIT:
		// start handling of indicator
		indicator_init();
		// initialise the register
		registerSensor_init();
		// start the daylight sensor
		if (veml6030_init() != ILB_HAL_OK)
		{
			// an error occured (we cannot talk to the sensor)
			sensorState = SENSOR_STATE_ERROR;
			break;
		}
		// proceed to next state
		sensorState = SENSOR_STATE_WAITING_FOR_START;
		break;

	case SENSOR_STATE_WAITING_FOR_START:
		if (startOperationReceived == 1)
		{
			sensorState = SENSOR_STATE_RUNNING;
		}
		break;

	case SENSOR_STATE_RUNNING:

		// check if we have pending motion events that should be published
		if (IS_BIT_SET(registerSettings[REGISTER_SETTINGS_REPORTING], REGISTER_SETTINGS_REPORT_MOTION) &&
			IS_BIT_SET(registerReadings[REGISTER_READING_MOTION_DAYLIGHT_STATUS], REGISTER_READING_MOTION_EVENT))
			{
				// send the message
				ILBMessageStruct report;
				report.address = addressing_getAssignedAddress();
				report.identifier = ILB_GENERATE_IDENTIFIER;
				report.messageType = ILB_MESSAGE_COMMAND;
				report.payloadSize = 2;
				report.payload[0] = ILB_REPORT_MOTION_DAYLIGHT_EVENT;
				report.payload[1] = ILB_REPORT_MOTION_DAYLIGHT_MOTION_FLAG;
				messaging_sendMessage(&report, reportCallback, SETTING_REPORT_TIMEOUT_MS, reportTimeout);

				// unset flag
				registerReadings[REGISTER_READING_MOTION_DAYLIGHT_STATUS] &= ~REGISTER_READING_MOTION_EVENT;
			}
		// check if we should report dayligh level crossing
		if (IS_BIT_SET(registerSettings[REGISTER_SETTINGS_REPORTING], REGISTER_SETTINGS_REPORT_DAYIGHT_THRESHOLD_CROSSING))
		{
			// decide if we are below threshold
			if ((registerReadings[REGISTER_READING_DAYLIGHT_UNIFLTERED_MSB] > registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_MSB] &&
				registerReadings[REGISTER_READING_DAYLIGHT_UNFILTERED_LSB] > registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_HIGH_LSB]) ||
				(registerReadings[REGISTER_READING_DAYLIGHT_UNIFLTERED_MSB] < registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_MSB] &&
				registerReadings[REGISTER_READING_DAYLIGHT_UNFILTERED_LSB] < registerSettings[REGISTER_SETTINGS_DAYLIGHT_THRESHOLD_LOW_LSB]))
			{
				// we are either above or below the threshold. Report reading
				ILBMessageStruct report;
				report.address = addressing_getAssignedAddress();
				report.identifier = ILB_GENERATE_IDENTIFIER;
				report.messageType = ILB_MESSAGE_COMMAND;
				report.payloadSize = 3;
				report.payload[0] = ILB_REPORT_MOTION_DAYLIGHT_EVENT;
				report.payload[1] = registerReadings[REGISTER_READING_DAYLIGHT_UNIFLTERED_MSB];
				report.payload[2] = registerReadings[REGISTER_READING_DAYLIGHT_UNFILTERED_LSB];

				// send message
				messaging_sendMessage(&report, reportCallback, SETTING_REPORT_TIMEOUT_MS, reportTimeout);
			}

		}
		// handle button
		button_getHoldTime(&buttonHoldTime);
		// check if button was pressed
		if (buttonHoldTime != BUTTON_IDLE)
		{
			// decide if we need to send the event
			ILBMessageStruct report;
			report.address = addressing_getAssignedAddress();
			report.identifier = ILB_GENERATE_IDENTIFIER;
			report.messageType = ILB_MESSAGE_COMMAND;
			report.payloadSize = 2;
			report.payload[0] = ILB_REPORT_BUTTON_EVENT;
			report.payload[1] = (buttonHoldTime & 0xFF);

			// send
			messaging_sendMessage(&report, reportCallback, SETTING_REPORT_TIMEOUT_MS, reportTimeout);

			// reset flag
			buttonHoldTime = BUTTON_IDLE;
		}

		break;

	case SENSOR_STATE_REPORTING:
		break;

	case SENSOR_STATE_ERROR:
		break;
	}
}

//
//	message handling
//
/**
 * Handler for commands directed to devcie
 * @param message
 */
void libLogic_commandHandler(ILBMessageStruct* message)
{
	switch (message->payload[0])
	{
	case ILB_START_OPERATION:
		startOperationReceived = 1;
		break;

	default:
		break;
	}
}

/**
 * Handler for read handler directed to device
 * @param message
 */
void libLogic_readHandler(ILBMessageStruct* message)
{
	registerSensor_read(message);
}

/**
 * Handler for write handler directed to device
 * @param message
 */
void libLogic_writeHandler(ILBMessageStruct* message)
{
	registerSensor_write(message);
}

//
//	Hardware interface
//

/**
 * External interrupt handling of hardware
 *
 * @param GPIO_Pin
 */
void HAL_GPIO_EXTI_Callback (uint16_t GPIO_Pin)
{
	// catch events from the motion sensor
	if (GPIO_Pin == DIRECT_LINK_Pin) {
		// reset the pin: acknowledge the event
		PYQ1548_clearInterrupt();
		// set flags
		registerReadings[REGISTER_READING_MOTION_DAYLIGHT_STATUS] |= REGISTER_READING_MOTION_EVENT;
	}
}

/**
 * Callback to report of daylight level or motion event
 *
 * @param message
 */
void reportCallback(ILBMessageStruct* message)
{

}

/**
 * Callback to timed out answer to report of motion event or daylight level
 */
void reportTimeout(void)
{

}
