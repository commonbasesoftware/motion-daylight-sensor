/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define NC_Pin GPIO_PIN_14
#define NC_GPIO_Port GPIOC
#define NCC15_Pin GPIO_PIN_15
#define NCC15_GPIO_Port GPIOC
#define LED_RED_Pin GPIO_PIN_0
#define LED_RED_GPIO_Port GPIOA
#define LED_GREEN_Pin GPIO_PIN_1
#define LED_GREEN_GPIO_Port GPIOA
#define LED_BLUE_Pin GPIO_PIN_2
#define LED_BLUE_GPIO_Port GPIOA
#define DIRECT_LINK_Pin GPIO_PIN_3
#define DIRECT_LINK_GPIO_Port GPIOA
#define DIRECT_LINK_EXTI_IRQn EXTI2_3_IRQn
#define SERIAL_IN_Pin GPIO_PIN_4
#define SERIAL_IN_GPIO_Port GPIOA
#define BUTTON_IN_Pin GPIO_PIN_5
#define BUTTON_IN_GPIO_Port GPIOA
#define NCA6_Pin GPIO_PIN_6
#define NCA6_GPIO_Port GPIOA
#define NCA7_Pin GPIO_PIN_7
#define NCA7_GPIO_Port GPIOA
#define NCB0_Pin GPIO_PIN_0
#define NCB0_GPIO_Port GPIOB
#define NCB1_Pin GPIO_PIN_1
#define NCB1_GPIO_Port GPIOB
#define NCB2_Pin GPIO_PIN_2
#define NCB2_GPIO_Port GPIOB
#define ILB_TX_ENABLE_Pin GPIO_PIN_8
#define ILB_TX_ENABLE_GPIO_Port GPIOA
#define ILB_TX_Pin GPIO_PIN_9
#define ILB_TX_GPIO_Port GPIOA
#define ILB_RX_Pin GPIO_PIN_10
#define ILB_RX_GPIO_Port GPIOA
#define NCA11_Pin GPIO_PIN_11
#define NCA11_GPIO_Port GPIOA
#define NCA12_Pin GPIO_PIN_12
#define NCA12_GPIO_Port GPIOA
#define NCA15_Pin GPIO_PIN_15
#define NCA15_GPIO_Port GPIOA
#define NCB3_Pin GPIO_PIN_3
#define NCB3_GPIO_Port GPIOB
#define NCB4_Pin GPIO_PIN_4
#define NCB4_GPIO_Port GPIOB
#define LIGHT_INT_Pin GPIO_PIN_5
#define LIGHT_INT_GPIO_Port GPIOB
#define BOOT_CHARGE_Pin GPIO_PIN_8
#define BOOT_CHARGE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
